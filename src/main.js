import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

//SETTINIG GLOBAL CONFIGURATION-it saves time 
//Setting a global configuration
axios.defaults.baseURL = 'https://vue-axios-8d511.firebaseio.com'
//Setting headers and using the common property to enable it access all requests
axios.defaults.headers.common['Authorization'] = 'fasfdsa'
//Setting headers for a specific request
axios.defaults.headers.get['Accepts'] = 'application/json'

//Setting interceptors
const reqInterceptor = axios.interceptors.request.use(config => {
  console.log('Request Interceptor', config)
  return config
})
const resInterceptor = axios.interceptors.response.use(res => {
  console.log('Response Interceptor', res)
  return res
})

//To remove the interceptors
axios.interceptors.request.eject(reqInterceptor)
axios.interceptors.response.eject(resInterceptor)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
