import axios from 'axios'

//Creating a custom instance
const instance = axios.create({
    baseURL: 'https://vue-axios-8d511.firebaseio.com'
})

//set headers on instance
instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance